create database agm;
#drop database agm;
use agm;

CREATE TABLE usuarios (
    idUsuario INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    nome VARCHAR(80) NOT NULL,
    email VARCHAR(80) NOT NULL UNIQUE,
    senha VARCHAR(80) NOT NULL
);


CREATE TABLE publicacoes (
    idPublicacoes INT PRIMARY KEY AUTO_INCREMENT,
    publiText MEDIUMTEXT,
    publiImg varchar(450),
    usuarioId INT NOT NULL,
    FOREIGN KEY (usuarioId)
        REFERENCES usuarios (idUsuario)
);

CREATE TABLE comentarios (
    comentarios MEDIUMTEXT,
    idPublicacoes INT ,
    usuarioEmail VARCHAR(80),
    FOREIGN KEY (idPublicacoes)
        REFERENCES publicacoes (idPublicacoes),
    FOREIGN KEY (usuarioEmail)
        REFERENCES usuarios (email),
    idComentario INT PRIMARY KEY AUTO_INCREMENT
);

CREATE TABLE adm (
    admEmail VARCHAR(80) PRIMARY KEY,
    FOREIGN KEY (admEmail)
        REFERENCES usuarios (email)
);

CREATE TABLE eventos (
    idEvento INT PRIMARY KEY AUTO_INCREMENT,
    dataEvento DATETIME,
    nomeEvento VARCHAR(80),
    descricao MEDIUMTEXT,
    admUsuarioEmail VARCHAR(80),
    FOREIGN KEY (admUsuarioEmail)
        REFERENCES adm (admEmail)
);

drop table usuarios;

drop table publicacoes;

drop table comentarios;

SELECT 
    *
FROM
    usuarios;
    
SELECT 
    *
FROM
    publicacoes;
    
SELECT 
    *
FROM
    comentarios;
    
    SELECT 
    *
FROM
    usuarios
WHERE
    email LIKE 'email@teste.com'
        OR nome LIKE '\'.$testeNOME.\''
LIMIT 1;

INSERT INTO usuarios(nome,email,senha) values
("b","teste@jpg.com","secreto");

INSERT INTO publicacoes(publiText,publiImg,usuarioId) values 	
("usuario 5","",5);

INSERT INTO comentarios(comentarios,idPublicacoes,usuarioEmail) values
("Este é um comentario de texto sobre o tamanho de otexto",1,"expudim@gmail.com");