#create database alunos;
use alunos;

create table aluno(
matricula int primary key auto_increment,
nome varchar(150),
nota1 double not null,
nota2 double not null,
nota3 double not null);
drop table aluno;
insert into aluno(nome,nota1,nota2,nota3)
values ('pedro',5.6,8.8,9.3),
		('aluna',6.3,9.0,7.6),
        ('carlos',1.5,4.5,2.4),
		('bruno',10,9.3,9.9);

       select * from aluno;
