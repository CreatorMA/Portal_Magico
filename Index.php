<!DOCTYPE HTML>
<!--
	Stellar by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
<?php
    include_once "conf/default.inc.php";
    require_once "conf/Conexao.php";
    include_once "functions/background.php";
    session_start();

?>
	<head>
		<title>Nosso Portal</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="is-preload">

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Header -->
					<header id="header" class="alt">
						<span class="logo"><img src="" alt="" /></span>
						<h1>Portal Mágico</h1>
						<p>Feira Literária Mágica</p>
					</header>

				<!-- Nav -->
        <form class="" action="action.php" method="post">

					<nav id="nav">
						<ul style="text-align:left">
							<li><a href="generic.html" class="active">Casas</a></li>
							<div style="float:right;">
                <?php echo isset($_SESSION['idUsuario']) ? '<li><button type="submit" name="acao" value="deslogar" class="active" style="background:black">Sair</button></li><li><a href="perfil/perfil.php">Ver perfil</a></li>': '<li><a style="text-align:right" href="main/log.php">Entrar</a></li>'; ?>

								<li><a style="text-align:right" href="main/cad.php">Cadastrar</a></li>
							</div>
						</ul>
					</nav>

        </form>
				<!-- Main -->
					<div id="main">

						<!-- Content -->
							<section id="content" class="main">

                <!--POSTAGEM ------------------------------------------------------------------>
                <?php if (isset($_SESSION['idUsuario'])): ?>

                  <h2>Poste algo!</h2>
                <form action="action.php" method="post">

                    <textarea name="text" rows="4" cols="40" placeholder="Oque você esta pensando?"></textarea>
                    <br>

                    <button type="submit" name="acao" value="postar">POSTAR</button>
                <img scr='img/test.jpg'>

                  </form>
                  <br>
                <?php endif; ?>

								<!-- Text -->
									<section>
                    <form method="post">

<?php
$show = isset($_POST['show']) ? $_POST['show'] : "";

		    $pdo = Conexao::getInstance();
		    $consulta = $pdo->query("SELECT * FROM publicacoes");

		    while ($row = $consulta->fetch(PDO::FETCH_ASSOC)){

		      $user = buscarDados($row['usuarioId']);

		      $coment = $pdo->query("SELECT * FROM comentarios WHERE idPublicacoes = ".$row['idPublicacoes']."");

// APRESENTANDO POST ---------------------------------------------------------->

			   echo '<p><code>'.$user['nome'].' | '.$user['email'].'</code></p>';
			   echo $row['publiImg'] != null ? '<img src="img/test.jpg" alt="" width="300px">':"";
			   echo "<p >{$row['publiText']}</p>\n";


         if(!isset($_SESSION['idUsuario']))
         echo "<hr>";

//COMENTANDO / MOSTRANDO COMENTARIOS ------------------------------------------>

			          if(isset($_SESSION['idUsuario'])){
			            if($show == $row['idPublicacoes']){

			          while ($linha = $coment->fetch(PDO::FETCH_ASSOC)){
			            echo '<code>'.$linha['usuarioEmail']."</code>  {$linha['comentarios']}<br>";
			            }



			              echo "\n\n".'</form><form method="post" action="action.php">  <textarea name="coment" rows="3" cols="30" placeholder="Comente aqui!"></textarea>'."\n";
			              echo '<button type="submit" name="acao" value="comentar"> comentar</button><br></form><form method="post">';
			              echo '<button type="submit" name="show" value="false" >Mostrar Menos</button><br><hr>'."\n";
			                $_SESSION['publicacao'] = $row['idPublicacoes'];
			              }
			              else
			              echo '<button type="submit" name="show" value="'.$row['idPublicacoes'].'" >Mostrar Mais</button><br><hr>';
			            }
			          }

 ?>

</form>
									</section>

							</section>

					</div>

				<!-- Footer -->
				<footer id="footer">
					<section>
						<h2>Sobre</h2>
						<p>A feira literária que chegou para, de  uma forma criativa e lúdica, despertar o interesse de jovens e adultos no universo da leitura.
				Traremos o mundo mágico para nossa realidade através de brincadeiras, livros, artesanatos, produtos temáticos, espaço kids, cenografia profissional, comidas e bebidas diversas, as quais estarão disponíveis para que os participantes possam se divertir.
				Ah! Não podemos esquecer de nossos cosplayers, que farão parte da ambientação desse universo.
				Evento Inspirado no Mundo Mágico de Harry Potter, usando o livro "Harry Potter e a Pedra Filosofal" como temática.</p>

					</section>
					<section>
						<h2>Entrar em contato :</h2>
						<dl class="alt">
							<dt>Telefone</dt>
							<dd>(61) 99924-2223</dd>
							<dt>Email</dt>
							<dd><a href="portalmagicobsb@gmail.com">portalmagicobsb@gmail.com</a></dd>
						</dl>
						<ul class="icons">
							<li><a href="https://www.facebook.com/PortalMagicoBsb/" class="icon brands fa-facebook-f alt"><span class="label">Facebook</span></a></li>
						</ul>
					</section>
					<p class="copyright">Design: <a href="https://html5up.net">HTML5 UP</a>.</p>
				</footer>

			</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>
