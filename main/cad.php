<!DOCTYPE HTML>
<!--
	Identity by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<?php
	  include_once "../functions/basic.php";
	  include_once "../conf/default.inc.php";
	  require_once "../conf/Conexao.php";

	  $testeNOME = isset($_POST['nome']) ? $_POST['nome'] : null;
	  $testeEMAIL = isset($_POST['email']) ? $_POST['email'] : null;
	  $testeSENHA = isset($_POST['senha']) ? $_POST['senha'] : null;

	  session_start();



		?>
		<title>Cadastro</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="is-preload">

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Main -->
					<section id="main">
						<header>
							<h1>Cadastre-se!	</h1>
						</header>
							<hr />
						<form method="post" action="../action.php">
							<div class="fields">
								<div class="field">
									<input type="text" name="nome" placeholder="Nome" required />
								</div>
								<div class="field">
									<input type="email" name="email" placeholder="Email" required />
								</div>
								<div class="field">
									<input type="text" name="senha" placeholder="Senha" required />
								</div>
							</div>
							<ul class="actions special">
								<li><input type="submit" class="button" name="acao" value="cadastrar"></li>
								<li></form><form method="post" action="../Index.php"><input type="submit" class="button" value="voltar"><a href="../Index.php"></button></form></li>
							</ul>
							<div class="field">
								<?php
								if(isset($_GET['erro']))
								  if($_GET['erro'] == "23000")
								  echo "<p>usuario ja cadastrado</p>";
								 ?>
							</div>
						</form>


					</section>

				<!-- Footer -->
					<footer id="footer">
						<ul class="copyright">
							Design: <a href="http://html5up.net">HTML5 UP</a></li>
						</ul>
					</footer>

			</div>

		<!-- Scripts -->
			<script>
				if ('addEventListener' in window) {
					window.addEventListener('load', function() { document.body.className = document.body.className.replace(/\bis-preload\b/, ''); });
					document.body.className += (navigator.userAgent.match(/(MSIE|rv:11\.0)/) ? ' is-ie' : '');
				}
			</script>

	</body>
</html>
