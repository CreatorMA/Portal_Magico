<?php

 include_once "conf/default.inc.php";
 require_once "conf/Conexao.php";

function dadosForm(){
    $dados = array();
    if(isset($_POST['nome']))
    $dados['nome'] = $_POST['nome'];
    if(isset($_POST['email']))
    $dados['email'] = $_POST['email'];
    if(isset($_POST['senha']))
    $dados['senha'] = $_POST['senha'];

    return $dados;
}

 function buscarDados($cod){
     $pdo = Conexao::getInstance();
     $consulta = $pdo->query("SELECT * FROM usuarios WHERE idUsuario = $cod");
     $dados = array();
     while ($linha = $consulta->fetch(PDO::FETCH_ASSOC)) {
         $dados['idUsuario'] = $linha['idUsuario'];
         $dados['nome'] = $linha['nome'];
         $dados['email'] = $linha['email'];
         $dados['senha'] = $linha['senha'];
     }
     return $dados;
 }

 function uploadIMG($img){

   $destino = "img/".$img;
   move_uploaded_file($img,$destino);
   return $destino;
 }
?>
